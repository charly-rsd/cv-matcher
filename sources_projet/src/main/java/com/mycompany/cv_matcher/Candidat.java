/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cv_matcher;

import java.util.ArrayList;

/**
 *
 * @author charl
 */
public class Candidat {
    private Identity identite;
    private ArrayList<Formation_candidat> formations;
    private ArrayList<Experience> experiences;
    private ArrayList<Langue> langues;
    private ArrayList<Competence> competences;
    private ArrayList<Loisir> loisirs;
    private ArrayList<Soft_skill> soft_skills;

    public Candidat() {
        formations = new ArrayList<>();
        experiences = new ArrayList<>();
        competences = new ArrayList<>();
        langues = new ArrayList<>();
        loisirs = new ArrayList<>();
    }

    public Candidat(Identity identite, ArrayList<Formation_candidat> formations, ArrayList<Experience> experiences, ArrayList<Langue> langues, ArrayList<Competence> competences, ArrayList<Loisir> loisirs, ArrayList<Soft_skill> soft_skills) {
        
        this.identite = identite;
        this.formations = formations;
        this.experiences = experiences;
        this.langues = langues;
        this.competences = competences;
        this.loisirs = loisirs;
        this.soft_skills = soft_skills;
    }
    
    

    public Identity getIdentite() {
        return identite;
    }

    public void setIdentite(Identity identite) {
        this.identite = identite;
    }
    
    public String getPrenom(){
        return identite.getPrenom();
    }
    
    public String getNom(){
        return identite.getNom();
    }

    public ArrayList<Formation_candidat> getFormations() {
        if(formations == null)
            return new ArrayList<>();
        return formations;
    }

    public void addFormations(Formation_candidat formation) {
        this.formations.add(formation);
    }
    
    public void removeFormations(int index){
        this.formations.remove(index);
    }
    
    public void addExperience(Experience exp) {
        this.experiences.add(exp);
    }
    
    public void removeExperiences(int index){
        this.experiences.remove(index);
    }
    public void removeCompetences(int index){
        this.competences.remove(index);
    }
    public void removeLangues(int index){
        this.langues.remove(index);
    }

    public ArrayList<Experience> getExperiences() {
        if(experiences == null)
            return new ArrayList<>();
        return experiences;
    }

    public void setExperiences(ArrayList<Experience> experiences) {
        this.experiences = experiences;
    }

    public ArrayList<Langue> getLangues() {
        return langues;
    }

    public void addLangues(Langue langue) {
        this.langues.add(langue);
    }

    public ArrayList<Competence> getCompetences() {
        if(competences == null)
            return new ArrayList<>();
        return competences;
    }

    public void addCompetences(Competence competence) {
        this.competences.add(competence);
    }

    public ArrayList<Loisir> getLoisirs() {
        return loisirs;
    }

    public void addLoisirs(Loisir loisir) {
        this.loisirs.add(loisir);
    }

    public ArrayList<Soft_skill> getSoft_skills() {
        return soft_skills;
    }

    public void setSoft_skills(ArrayList<Soft_skill> soft_skills) {
        this.soft_skills = soft_skills;
    }
    
    
}
