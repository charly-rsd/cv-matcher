/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cv_matcher;

/**
 *
 * @author charl
 */
public class Formation_offre {
     private String domaine;
     private Level niveau;

     public Formation_offre() {}


    public String getDomaine() {
        return domaine;
    }

    public void setDomaine(String domaine) {
        this.domaine = domaine;
    }

    public Level getNiveau() {
        return niveau;
    }

    public Formation_offre(String domaine, Level niveau) {
        this.domaine = domaine;
        this.niveau = niveau;
    }

    public void setNiveau(String niveau) {
        this.niveau = Level.valueOf(niveau);
    }
}
