/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cv_matcher;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author charl
 */
public class Matcher {
    private Offre offre;
    private HashMap<Candidat,Integer> candidats;

    public Matcher(Offre offre, HashMap<Candidat, Integer> candidats) {
        this.offre = offre;
        this.candidats = candidats;
    }
    
    
    
    
    public HashMap<Candidat,Integer> match(){
        HashMap<Candidat,Integer> newList = new HashMap<Candidat,Integer>();
        candidats.forEach(
                (k,v)->  newList.put(k,calcul_score(k))
        );
        return newList;
    }

    private Integer calcul_score(Candidat candidat) {
        int i = 0;
        System.out.println(candidat.getPrenom() + " " + candidat.getNom());
        if(!candidat.getCompetences().isEmpty() || candidat.getCompetences()!= null){
        for(Competence c_offre : offre.getCompetences()){
            for(Competence c_candidat : candidat.getCompetences()){
                if(c_offre.getCompetence().equalsIgnoreCase(c_candidat.getCompetence())){
                    i=i+1;
                }
            }
        }
        }
        if(!candidat.getExperiences().isEmpty() || candidat.getExperiences() != null){
        for(Experience experience : candidat.getExperiences()){
            if(experience.getDomaine().equalsIgnoreCase(offre.getFormation().getDomaine())){
               i=i+3;
            }
        }
        }
        if(!candidat.getFormations().isEmpty() || candidat.getFormations() != null){
        for(Formation_candidat formation : candidat.getFormations()){
            if(formation.getDomaine().equalsIgnoreCase(offre.getFormation().getDomaine())){
                i= i + formation.getNiveau().ordinal() +1;
            }
   
        }
        }
        return i;
    }
    
      
    
}
