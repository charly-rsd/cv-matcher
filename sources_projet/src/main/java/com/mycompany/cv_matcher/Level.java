/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cv_matcher;

/**
 *
 * @author charl
 */
public enum Level {
    LICENCE1,
    LICENCE2,
    LICENCE3,
    DUT1,
    DUT2,
    MASTER1,
    MASTER2
}
