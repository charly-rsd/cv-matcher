/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cv_matcher;

/**
 *
 * @author charl
 */
public class Competence {
    private String competence;

    public Competence() {}

    public Competence(String competence) {
        this.competence = competence;
    }

    public String getCompetence() {
        return competence;
    }

    public void setCompetence(String competence) {
        this.competence = competence;
    }
    
}
