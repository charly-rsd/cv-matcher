/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cv_matcher;

import java.util.ArrayList;

/**
 *
 * @author charl
 */
public class Offre {
    private String nom;
    private String Entreprise;
    private Formation_offre formation;
    private ArrayList<Competence> competences ;
    private String description;
    private Soft_skill softskills;

    

    public String getEntreprise() {
        return Entreprise;
    }

    public void setEntreprise(String Entreprise) {
        this.Entreprise = Entreprise;
    }

    public ArrayList<Competence> getCompetences() {
        return competences;
    }

    public void setCompetences(ArrayList<Competence> competences) {
        this.competences = competences;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Formation_offre getFormation() {
        return formation;
    }

    public void setFormation(Formation_offre formation) {
        this.formation = formation;
    }

  
    public void setCompetence(ArrayList<Competence> competences) {
        this.competences = competences;
    }

    public void addCompetence(Competence competence) {
        this.competences.add(competence);
    }
  public void removeCompetence(Competence competence) {
        this.competences.remove(competence);
    }
   

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Soft_skill getSoftskills() {
        return softskills;
    }

    public void setSoftskills(Soft_skill softskills) {
        this.softskills = softskills;
    }
}
