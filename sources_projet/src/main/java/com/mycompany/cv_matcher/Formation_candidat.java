/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cv_matcher;

/**
 *
 * @author charl
 */
public class Formation_candidat  extends Formation_offre{
     private String debut;
    private String fin;
    private String etablissement;

    public Formation_candidat(String domaine, Level niveau) {
        super(domaine, niveau);
    }
    
    public Formation_candidat() {}


  


    
    public String getDebut() {
        return debut;
    }

    public void setDebut(String debut) {
        this.debut = debut;
    }

    public String getFin() {
        return fin;
    }

    public void setFin(String fin) {
        this.fin = fin;
    }

    public String getEtablissement() {
        return etablissement;
    }

    public void setEtablissement(String etablissement) {
        this.etablissement = etablissement;
    }


    /*@Override
    public void setNiveau(String niveau) {
        super.niveau =Level.valueOf(niveau);
    }
      public S getNiveau() {
        return niveau;
    }*/
}
